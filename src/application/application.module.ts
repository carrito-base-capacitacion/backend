import { Module } from '@nestjs/common'
import { ParametroModule } from './parametro/parametro.module'
import { TiendaModule } from './tienda/tienda.module';

@Module({
  imports: [ParametroModule, TiendaModule],
})
export class ApplicationModule {}
