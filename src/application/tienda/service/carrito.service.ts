import { Injectable } from '@nestjs/common';
import { CarritoDTO } from '../dto';
import { CarritoRepository, ProductoRepository } from '../repository';
import { Carrito, Producto } from '../entity';
import { BaseService } from 'src/common/base';

@Injectable()
export class CarritoService extends BaseService{
  constructor(
    private carritoRepository: CarritoRepository,
    private productoRespository: ProductoRepository,
  ) {
    super();
  }

  async listarCarritoDeCompra(codigo: string) {
    return await this.carritoRepository.listarPorId(codigo);
    // return news.map((newEntity) => this.mapper.entityToDto(newEntity));
  }

  async listarTodosLosCarritos(idUsuario: string) {
    const carrito: Carrito[] = await this.carritoRepository.listar(idUsuario);
    // return news.map((newEntity) => this.mapper.entityToDto(newEntity));
    return carrito;
  }

  async quitarProductoDelCarrito(idProducto: string) {
    return await this.carritoRepository.quitarProducto(idProducto);
  }

  async aumentarProducto(codigo: string, usuarioAuditoria: string) {
    return await this.carritoRepository.operarCantidad(codigo, true, usuarioAuditoria)
  }

  async restarProducto(codigo: string, usuarioAuditoria: string) {
    return await this.carritoRepository.operarCantidad(codigo, false, usuarioAuditoria)
  }

  async productoExisteEnCarrito(idUsuario: string, idProducto: string) {
    const carrito: Carrito[] = await this.listarTodosLosCarritos(idUsuario);
    this.logger.info('333333333333333333333333333333333---', idProducto)
    this.logger.info(carrito, 'fin de la lista')
    const res = carrito.find(item => item.producto.id === idProducto);
    this.logger.info(res, 'la respuesta')
    if (res && Object.keys(res).length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
