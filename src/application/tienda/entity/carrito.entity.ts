import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Producto } from './producto.entity';
import { Venta } from './venta.entity';
import { AuditoriaEntity } from 'src/common/entity/auditoria.entity';

@Entity({ name: 'carrito', schema: process.env.DB_SCHEMA_TIENDA })
export class Carrito extends AuditoriaEntity{
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Carrito',
  })
  id: string;

  @Column({ type: 'int', comment: 'Cantidad del producto' })
  cantidad: number;

  @ManyToOne(() => Producto, (producto) => producto.id)
  @JoinColumn({ name: 'id_producto' })
  producto: Producto;

  @ManyToOne(() => Venta, (venta) => venta.id)
  @JoinColumn({ name: 'id_venta' })
  venta: Venta;

  constructor() {
    super();
  }
}
