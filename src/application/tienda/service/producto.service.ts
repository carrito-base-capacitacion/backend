import { Injectable } from '@nestjs/common';
import { ProductoRepository } from '../repository';
import { ProductoDTO } from '../dto';
import { Producto } from '../entity';

@Injectable()
export class ProductoService {
  constructor(
    private productoRespository: ProductoRepository,
  ) {}

  async listar() {
    const productos: Producto[] = await this.productoRespository.listar();
    return productos;
  }

  async listarPorId(id: string): Promise<Producto | null> {
    return await this.productoRespository.listarPorId(id);
  }

  async crear(productoDTO: ProductoDTO, usuarioAuditoria: string) {
    const producto = new Producto();
    producto.nombre = productoDTO.nombre;
    producto.descripcion = productoDTO.descripcion;
    producto.imagen = productoDTO.imagen;
    producto.precio = productoDTO.precio;
    producto.usuarioCreacion = usuarioAuditoria;
    return await this.productoRespository.crear(producto);
  }

  async eliminar(id: string): Promise<void> {
    await this.productoRespository.eliminar(id);
  }

  async editar(id: string, dto: ProductoDTO, usuarioAuditoria: string) {
    let producto = new Producto();
    const aux = await this.listarPorId(id);
    if(aux) {
      producto = aux;
    } else {
      return;
    }
    producto.nombre = dto.nombre?dto.nombre:producto.nombre;
    producto.descripcion = dto.descripcion?dto.descripcion:producto.descripcion;
    producto.imagen = dto.imagen?dto.imagen:producto.imagen;
    producto.precio = dto.precio?dto.precio:producto.precio;
    producto.usuarioModificacion = usuarioAuditoria;
    return await this.productoRespository.crear(producto);
  }
}
