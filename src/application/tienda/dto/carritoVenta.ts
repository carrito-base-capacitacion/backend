import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { Type } from 'class-transformer';
import { ProductoDTO } from './producto.dto';

export class CarritoVentaDTO {
  @ApiProperty()
  @IsString()
  idUsuario: string;

  @ApiProperty()
  @IsString()
  idProducto: string;

  @ApiProperty()
  @IsNumber()
  cantidad: number;
}
