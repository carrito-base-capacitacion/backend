import { ConflictException, Injectable } from '@nestjs/common';
import { CarritoDTO } from '../dto';
import { CarritoRepository, ProductoRepository } from '../repository';
import { Carrito, Producto, Venta } from '../entity';
import { VentaRepository } from '../repository/venta.repository';
import { UsuarioRolRepository } from 'src/core/authorization/repository/usuario-rol.repository';
import { UsuarioRol } from 'src/core/authorization/entity/usuario-rol.entity';
import { BaseService } from 'src/common/base';
import { VentaEnum } from '../enums/venta.enum';
import { CarritoService } from './carrito.service';

@Injectable()
export class VentaService extends BaseService {
  constructor(
    private ventaRepository: VentaRepository,
    private productoRespository: ProductoRepository,
    private carritoRespository: CarritoRepository,
    private carritoService: CarritoService,
  ) {
    super();
  }

  async agregarProductoAlCarrito(idUsuario: string, idProducto: string, cantidad: number, usuarioAuditoria: string) {
    const producto: Producto | null = await this.productoRespository.listarPorId(idProducto);
    let venta = await this.ventaRepository.obtenerPorIdUsuario(idUsuario);
    this.logger.info('-----------------------------------')
    this.logger.info(venta, producto)
    this.logger.info('-----------------------------------')
    if (!venta) {
      const ventaAux = new Venta();
      const usuario: UsuarioRol | null = await this.ventaRepository.obtenerUsuarioPorId(idUsuario);
      if (usuario) {
        ventaAux.usuario = usuario;
      }
      this.logger.info('-----------------------------------2')
      this.logger.info(venta)
      this.logger.info('-----------------------------------2')
      venta = await this.ventaRepository.crear(ventaAux, usuarioAuditoria);
      this.logger.info('-----------------------------------3')
      this.logger.info(venta)
      this.logger.info('-----------------------------------3')
    }
    const carrito = new Carrito();
    carrito.venta = venta;
    carrito.cantidad = cantidad;
    this.logger.info('-----------------------------------')
    this.logger.info(venta, producto, carrito)
    this.logger.info('-----------------------------------')
    const productoEnCarrito = await this.carritoService.productoExisteEnCarrito(idUsuario,idProducto)
    if (producto) {
      carrito.producto = producto;
      carrito.usuarioCreacion = usuarioAuditoria;
      const res = await this.carritoRespository.agregarProductoAlCarrito(carrito);
      return res;
    } else {
      throw new Error('No se agrego el producto al carrito');
    }
  }

  async actualizarEstadoVenta(codigo: string, estado: string, usuarioAuditoria: string) {
    return await this.ventaRepository.cambiarEstadoPorIdVenta(codigo, estado, usuarioAuditoria);
  }

  async obtenerResumenVentas() {
    return await this.ventaRepository.obtenerResumenVentas();
  }
}