import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { CarritoDTO, CarritoVentaDTO } from '../dto';
import { CarritoService, VentaService } from '../service';
import { BaseController } from 'src/common/base';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/core/authentication/guards/jwt-auth.guard';
import { CasbinGuard } from 'src/core/authorization/guards/casbin.guard';

@ApiTags('Parámetros')
@ApiBearerAuth()
@Controller('/carrito-de-compra')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class CarritoController extends BaseController {
  constructor(
    private carritoService: CarritoService,
    private ventaService: VentaService) {
    super();
  }

  // @Get()
  // async getAllNews(): Promise<NewDTO[]> {
  //   return await this.newsService.getAllNews();
  // }

  @Get(':id')
  async listarTodosLosCarritos(@Param('id') id: string) {
    const res = await this.carritoService.listarTodosLosCarritos(id);
    console.log(res)
    return res;
  }

  // @Get()
  // async listarCarritoDeCompra() {
  //   return await this.carritoService.listarCarritoDeCompra(id);
  // }

  //   @Get()
  //   async getNewsFilter(@Query() filter): Promise<NewDTO[]> {
  //     return await this.newsService.getNewsFilter(
  //       filter.title ? filter.title : null,
  //       filter.place ? filter.place : null,
  //       filter.year ? filter.year : null,
  //       filter.month ? filter.month : null,
  //     );
  //   }

  @Post()
  async createNews(@Req() req: Request, @Body() dto: CarritoVentaDTO) {
    const usuarioAuditoria = this.getUser(req)
    return await this.ventaService.agregarProductoAlCarrito(dto.idUsuario, dto.idProducto, dto.cantidad, usuarioAuditoria);
  }

  @Post(':id/cambiar-estado')
  async actualizarEstado(@Req() req: Request,@Query('estado') estado: string, @Param('id') id: string) {
    const usuarioAuditoria = this.getUser(req)
    return await this.ventaService.actualizarEstadoVenta(id, estado, usuarioAuditoria);
  }

  @Patch(':id/sumar')
  async agregarCantidad(@Req() req: Request, @Param('id') idCarrito: string) {
    const usuarioAuditoria = this.getUser(req)
    return await this.carritoService.aumentarProducto(idCarrito, usuarioAuditoria);
  }

  @Patch(':id/restar')
  async restarCantidad(@Req() req: Request, @Param('id') idCarrito: string) {
    const usuarioAuditoria = this.getUser(req)
    return await this.carritoService.restarProducto(idCarrito, usuarioAuditoria);
  }

  //   @Put(':id')
  //   async updateNews(
  //     @Param('id') id: string,
  //     @Body() news: NewDTO,
  //   ): Promise<NewDTO> {
  //     return await this.newsService.updateNews(id, news);
  //   }

  @Delete(':id')
  async deleteUser(@Param('id') id: string) {
    return await this.carritoService.quitarProductoDelCarrito(id);
  }
}
