import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Producto, Carrito, Venta } from './entity';
import { ProductoRepository, CarritoRepository, VentaRepository } from './repository';
import { CarritoController, ProductoController } from './controller';
import { CarritoService, ProductoService, VentaService } from './service';
import { VentasController } from './controller/ventas.controller';

@Module({
    imports: [TypeOrmModule.forFeature([Producto, Carrito, Venta])],
    controllers: [CarritoController, ProductoController, VentasController],
    providers: [
      CarritoService,
      ProductoService,
      CarritoRepository,
      ProductoRepository,
      VentaRepository,
      VentaService
    ],
    //   providers: [NewsService, NewMapper, NewsRepository],
    exports: [CarritoService, ProductoService, VentaService],
})
export class TiendaModule {}
