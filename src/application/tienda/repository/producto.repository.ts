import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Producto } from '../entity';
import { DeleteResult, FindOneOptions, FindOptions, Repository } from 'typeorm';
import { ExceptionsHandler } from '@nestjs/core/exceptions/exceptions-handler';

@Injectable()
export class ProductoRepository {
  constructor(
    @InjectRepository(Producto)
    private productoRepository: Repository<Producto>,
  ) {}

  async listar() {
    return this.productoRepository.find({
      order: {
        nombre: 'ASC',
      },
    });
  }

  async listarPorId(codigo: string) {
    return this.productoRepository.findOne({ where: { id: codigo } });
  }

  async crear(producto: Producto) {
    return this.productoRepository.save(producto);
  }

  async eliminar(codigo: string) {
    return this.productoRepository.delete(codigo);
  }
}
