import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { Type } from 'class-transformer';
import { ProductoDTO } from './producto.dto';

export class CarritoDTO {
  @ApiProperty()
  @IsUUID()
  id?: string;

  @ApiProperty()
  @IsOptional()
  @Type(() => ProductoDTO)
  producto: ProductoDTO;

  @ApiProperty()
  @IsNumber()
  cantidad: number;
}
