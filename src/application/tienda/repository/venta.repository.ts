import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Venta } from '../entity';
import { DataSource, DeleteResult, FindOneOptions, FindOptions, Repository } from 'typeorm';
import { ExceptionsHandler } from '@nestjs/core/exceptions/exceptions-handler';
import { UsuarioRol } from 'src/core/authorization/entity/usuario-rol.entity';
import dayjs from 'dayjs'
import { VentaEnum } from '../enums/venta.enum';

@Injectable()
export class VentaRepository {
  constructor(
    @InjectRepository(Venta)
    private ventaRepository: Repository<Venta>,
    private dataSource: DataSource
  ) {}

  async listar() {
    return this.ventaRepository.find();
  }

  async listarPorId(codigo: string): Promise<Venta | null>  {
    return this.ventaRepository.findOne({ where: { id: codigo } });
  }

  async crear(venta: Venta, usuarioAuditoria: string) {
    venta.estado = VentaEnum.EN_PROGRESO
    venta.usuarioCreacion = usuarioAuditoria
    return this.ventaRepository.save(venta);
  }

  async obtenerPorIdUsuario(codigo: string) {
    const aux = this.ventaRepository.createQueryBuilder('ventas')
    .leftJoinAndSelect('ventas.usuario','usuario')
    .where('usuario.id = :codigo', { codigo })
    .where(`ventas._estado = :estado`, {estado: VentaEnum.EN_PROGRESO})
    .getOne();
    console.log('-------------------------------')
    console.log('ddddddddddddddddddddd',aux)
    return aux;
  }

  async obtenerUsuarioPorId(idUsuario: string) {
    return await this.dataSource.getRepository(UsuarioRol).findOne({ where: { id: idUsuario } });
  }

  async cambiarEstadoPorIdVenta(codigo: string, estado: string, usuarioAuditoria: string) {
    const aux = await this.ventaRepository.findOne({ where: { id: codigo } });
    // const aux = await this.ventaRepository.createQueryBuilder('ventas')
    // .leftJoinAndSelect('ventas.usuario','usuario')
    // .where('ventas.id = :codigo', { codigo })
    // .getOne();
    if (aux) {
      aux.estado = estado;
      if(!estado) {
        aux.fechaCompra = dayjs().toDate()
      }
      aux.usuarioModificacion = usuarioAuditoria;
      console.log('2222222222222222222222',aux)
      return this.ventaRepository.save(aux);
    }
  }

  async obtenerResumenVentas() {
    try {
      const query = `
        SELECT
          v.id,
          p2.nombres,
          p2.primer_apellido,
          p2.segundo_apellido,
          p2.tipo_documento,
          p2.nro_documento,
          v.fecha_compra,
          SUM(c.cantidad * p.precio) AS total_pagado
        FROM tienda.ventas v
        INNER JOIN usuarios.usuarios_roles ur ON v.id_usuario = ur.id
        INNER JOIN tienda.carrito c ON c.id_venta = v.id
        INNER JOIN tienda.producto p ON p.id = c.id
        INNER JOIN usuarios.usuarios u ON ur.id_usuario = u.id
        INNER JOIN usuarios.personas p2 ON p2.id = u.id_persona
        WHERE v._estado = ${VentaEnum.VENDIDO}
        GROUP BY v.id, c.cantidad, p2.nombres, p2.primer_apellido, p2.segundo_apellido, p2.tipo_documento, p2.nro_documento
        ORDER BY v.fecha_compra DESC;
      `;

      const result = await this.dataSource.query(query);
      return result;
    } catch (error) {
      throw new Error('Error al ejecutar la consulta SQL: ' + error.message);
    }
  }
}
