import { AuditoriaEntity } from 'src/common/entity/auditoria.entity';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'producto', schema: process.env.DB_SCHEMA_TIENDA })
export class Producto extends AuditoriaEntity{
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Producto',
  })
  id: string;

  @Column({
    length: 50,
    type: 'varchar',
    unique: true,
    comment: 'Nombre del producto',
  })
  nombre: string;

  @Column({ length: 255, type: 'varchar', comment: 'Descripción del producto' })
  descripcion: string;

  @Column({
    length: 400,
    type: 'varchar',
    comment: 'Url de la imagen del producto',
  })
  imagen: string;

  @Column({ type: 'int', comment: 'Precio del producto' })
  precio: number;

  constructor() {
    super();
  }
}
