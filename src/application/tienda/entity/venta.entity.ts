import { AuditoriaEntity } from 'src/common/entity/auditoria.entity';
import { UsuarioRol } from 'src/core/authorization/entity/usuario-rol.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { VentaEnum } from '../enums/venta.enum';

@Entity({ name: 'ventas', schema: process.env.DB_SCHEMA_TIENDA })
export class Venta extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Ventas',
  })
  id: string;

  @Column({
    type: 'timestamp',
    nullable: true,
    comment: 'Fecha de compra',
    name: 'fecha_compra'
  })
  fechaCompra: Date;

  @ManyToOne(() => UsuarioRol, (usuario) => usuario.id)
  @JoinColumn({ name: 'id_usuario' })
  usuario: UsuarioRol;


  constructor() {
    super();
  }
}
