import { MigrationInterface, QueryRunner } from "typeorm";
import { Producto } from "../../src/application/tienda/entity";
import { USUARIO_SISTEMA } from "src/common/constants";

export class Seeds1706903458633 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const items = [
            {
                nombre: "Zapatos de varon",
                descripcion: "Talla 41",
                imagen: "https://m.media-amazon.com/images/I/61peSHJtH3L._AC_SX575_.jpg",
                precio: 23
            },
            {
                nombre: "Zapatos de mujer",
                descripcion: "Talla 38, estilo casual",
                imagen: "https://m.media-amazon.com/images/I/81P3O0xZqBL._AC_SX575_.jpg",
                precio: 30
            },
            {
                nombre: "Camiseta unisex",
                descripcion: "Color negro, talla M",
                imagen: "https://m.media-amazon.com/images/I/91iw-l4PS+L._AC_SX569_.jpg",
                precio: 15
            },
            {
                nombre: "Bolso de cuero",
                descripcion: "Color marrón, diseño elegante",
                imagen: "https://m.media-amazon.com/images/I/71YX4E-r8UL._AC_SX569_.jpg",
                precio: 50
            },
            {
                nombre: "Reloj digital",
                descripcion: "Resistente al agua, pantalla LED",
                imagen: "https://m.media-amazon.com/images/I/6156ix4oezL._AC_UL320_.jpg",
                precio: 40
            },
            {
                nombre: "Auriculares inalámbricos",
                descripcion: "Con cancelación de ruido, Bluetooth",
                imagen: "https://m.media-amazon.com/images/I/61TrbMmWyOL._AC_UY218_.jpg",
                precio: 60
            }
        ]
        const productos = items.map((item) => {
            const aux = new Producto();
            aux.nombre = item.nombre;
            aux.descripcion = item.descripcion;
            aux.imagen = item.imagen;
            aux.precio = item.precio;
            aux.estado = 'ACTIVO';
            aux.transaccion = 'SEEDS';
            aux.usuarioCreacion = USUARIO_SISTEMA;
            return aux;
          })
          await queryRunner.manager.save(productos)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
