import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { Type } from 'class-transformer';
import { ProductoDTO } from './producto.dto';
import { IsDate } from 'src/common/validation';

export class ResumenVentaDTO {
  @ApiProperty()
  @IsString()
  idVenta: string;

  @ApiProperty()
  @IsString()
  nombreUsuario: string;

  @ApiProperty()
  @IsDate()
  fechaCompra: Date;

  @ApiProperty()
  @IsNumber()
  cantidad: number;

}
