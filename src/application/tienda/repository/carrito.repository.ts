import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Carrito, Producto } from '../entity';
import { DeleteResult, FindOneOptions, FindOptions, Repository } from 'typeorm';
import { ExceptionsHandler } from '@nestjs/core/exceptions/exceptions-handler';
import { VentaEnum } from '../enums/venta.enum';
import { TiendaEnum } from '../enums/tienda.enum';

@Injectable()
export class CarritoRepository {
  constructor(
    @InjectRepository(Carrito)
    private carritoRepository: Repository<Carrito>,
  ) { }

  async listarPorId(codigo: string) {
    return this.carritoRepository.findOne({ where: { id: codigo } });
  }

  async listar(codigo: string) {
    //return this.carritoRepository.find();
    const carritos = await this.carritoRepository
      .createQueryBuilder('carrito')
      .leftJoinAndSelect('carrito.producto', 'producto')
      .leftJoinAndSelect('carrito.venta', 'venta')
      .leftJoinAndSelect('venta.usuario', 'usuario')
      .where('usuario.id = :codigo', { codigo })
      .andWhere(`venta._estado = :aux`, { aux: VentaEnum.EN_PROGRESO })
      .getMany();
    return carritos;
  }

  async operarCantidad(codigo: string, sumar: boolean, usuarioAuditoria: string) {

    try {
      const carrito = await this.listarPorId(codigo)
      if (carrito) {
        carrito.cantidad = sumar ? carrito.cantidad + 1 : carrito.cantidad - 1;
        carrito.usuarioModificacion = usuarioAuditoria;
        return this.carritoRepository.save(carrito);
      }
    } catch (error) {

    }
  }

  async agregarProductoAlCarrito(carrito: Carrito) {
    carrito.estado = TiendaEnum.ACTIVO
    return this.carritoRepository.save(carrito)
  }

  async quitarProducto(idCarrito: string) {
    return this.carritoRepository.delete(idCarrito)
  }
}
