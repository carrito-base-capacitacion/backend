import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { CarritoDTO, ProductoDTO } from '../dto';
import { CarritoService, ProductoService } from '../service';
import { stdoutWrite } from 'src/core/logger/tools';
import { id } from 'cls-rtracer';
import { BaseController } from 'src/common/base';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/core/authentication/guards/jwt-auth.guard';
import { CasbinGuard } from 'src/core/authorization/guards/casbin.guard';

@ApiTags('Productos')
@ApiBearerAuth()
@Controller('/productos')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class ProductoController extends BaseController{
  constructor(private productoService: ProductoService) {
    super();
  }

  @Get()
  async listarTodosLosProductos() {
    return await this.productoService.listar();
  }

  @Get(':id')
  async listarProductoPorId(@Param('id') id: string) {
    return await this.productoService.listarPorId(id);
  }

  @Post()
  async crearProducto(@Req() req: Request, @Body() productoDTO: ProductoDTO): Promise<ProductoDTO> {
    const usuarioAuditoria = this.getUser(req)
    this.logger.info(usuarioAuditoria);
    return await this.productoService.crear(productoDTO, usuarioAuditoria);
  }

  @Patch(':id')
  async updateNews(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() dto: ProductoDTO,
  ){
    const usuarioAuditoria = this.getUser(req)
    return await this.productoService.editar(id, dto, usuarioAuditoria);
  }

  @Delete(':id')
  async eliminarProducto(@Param('id') id: string): Promise<void> {
    return await this.productoService.eliminar(id);
  }
}
