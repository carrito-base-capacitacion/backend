import {
    Controller,
    Get,
    UseGuards,
  } from '@nestjs/common';
  import { VentaService } from '../service';
  import { BaseController } from 'src/common/base';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/core/authentication/guards/jwt-auth.guard';
import { CasbinGuard } from 'src/core/authorization/guards/casbin.guard';
  
  @ApiTags('Parámetros')
  @ApiBearerAuth()
  @Controller('/ventas')
  @UseGuards(JwtAuthGuard, CasbinGuard)
  export class VentasController extends BaseController {
    constructor(
      private ventaService: VentaService) {
      super();
    }
  
    @Get()
    async obtenerResumenVentas() {
      this.logger.info('sssssssssssss')
  
      const res = await this.ventaService.obtenerResumenVentas();
      this.logger.info(res, '-------------------------2')
      return res;
    }
  }
  